import React from "react";
import "./App.scss";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

import Reservations from "./components/Reservations/Reservations";

export default function App() {
  return (
    <div className="App">
      <Router>
        <nav>
          <ul>
            <li>
              <NavLink to="/Devices" activeClassName="active">
                Devices
              </NavLink>
            </li>
            <li>
              <NavLink to="/Reservations">Reservations</NavLink>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/Reservations">
            <Reservations />
          </Route>
          <Route path="/Devices">
                {/* <SingleDevice /> */}
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
