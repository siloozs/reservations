
import React , { useCallback    } from 'react'
import "react-datepicker/dist/react-datepicker.css";
import observableDeviceStore from '../../Store';
import { useInput  } from '../../utils';
import './Popup.scss';
import DatePicker from "react-datepicker";

import BtnAddReservation from '../BtnAddReservation/BtnAddReservation';
import { observer } from "mobx-react-lite";
import HoursOptions from '../HoursOptions/HoursOptions';


function Popup(props) {

  const { value: selectedDeviceIdx , bind: bindDeviceIdSelect } = useInput(0);
  const { value: fromTime  , bind: bindFromTime } = useInput(0);
  const { value: TargetTime  , bind: bindTargetTime } = useInput(0);



    const submitForm = useCallback( (evt) => {
            evt.preventDefault();
            observableDeviceStore.setReservation( parseInt(selectedDeviceIdx) ,{
              i: parseInt(fromTime),
              idxEnd: parseInt(TargetTime) 
           })
           props.onClick(); 

    } ,[props  ,selectedDeviceIdx , fromTime , TargetTime ])




  return (
    <div className="add-reservation-popup">
        <div id="popup1" className="overlay">
          <div className="popup">
            <h2>Add Reservation</h2>
            <button className="close" data-target={props.target} onClick={props.onClick}>&times;</button>
            <div className="content">
                  <form onSubmit={submitForm}>
                     <div className="single-form-field">
                        <label htmlFor="popup-date">Current Date</label>
                        <DatePicker dateFormat="dd/MM/yyyy" selected={observableDeviceStore.currentDateObj} onChange={date => observableDeviceStore.setDate(date)} />
                    </div>
                    <div className="single-form-field">
                          <label htmlFor="popup-device">Choose Device</label>
                          <select  {...bindDeviceIdSelect}>
                              {
                                observableDeviceStore.devices.map( (d,i) => <option key={i} value={i}>{d.name}</option> )
                              }
                          </select> 
                    </div>
                    <div className="single-form-field">
                          <label>Choose Time</label>
                         <div className="time-select">
                                  <div>From: 
                                    <select className="time-picker-hours" {...bindFromTime} >
                                       <HoursOptions options={observableDeviceStore.fulltimelineDevice(selectedDeviceIdx)} target="from" />
                                        
                                    </select>
                                  </div>
                                  <div>
                                    To:
                                    <select className="time-picker-hours"  {...bindTargetTime}>
                                    <HoursOptions options={observableDeviceStore.fulltimelineDevice(selectedDeviceIdx)}  target="to" fromTime={fromTime} />
                                    </select>
                                  </div>
                         </div>
                   </div>
                   <BtnAddReservation text="Save Reservation"/>
                </form>      
            </div>  
          </div>
        </div>
    </div>
  )
}



export default observer(Popup)