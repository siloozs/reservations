import React from "react";
import "./Timeline.scss";
import { observer } from "mobx-react-lite"
import SingleDeviceTracker from '../SingleDeviceTracker/SingleDeviceTracker';
import SingleDevice from '../SingleDevice/SingleDevice';
import { HoursTimeline } from "../../utils";
import observableDeviceStore from '../../Store';


const Timeline = (props) => {
  return (
   <> 
    <div className="timeline">
      <header>
        <div className="timeline-text-header device-names">Devices/Time</div>
        <div className="timeline-body">
          {observableDeviceStore.devices.map(d => <SingleDevice key={d.serial} device={d}/>)}
        </div>
      </header>
      <section className="timeline-tracker">
        <div className="timeline-tracker-inner">
          <div className="timeline-text-header">
            {
              <div className="timeline-hours">
                <ol>
                  {HoursTimeline.map((h, idx) => <li key={idx}>{h}</li>)}
                </ol>
                <div className="timeline-body-tracker">
                {observableDeviceStore.devices.map(d => <SingleDeviceTracker key={d.serial} reservations={d.reservations}  date={observableDeviceStore.currentDate}  currentUserID={observableDeviceStore.loggedUserId}/>)}    
                </div>
                
              </div>
            }
          </div>
        </div>
      </section>
    </div>
      <footer className="timeline-footer">
                    <div className="others">Reserved by others</div>
                    <div className="me">Reserved by me</div>
      </footer>
    </>
  );
};

export default observer(Timeline);
