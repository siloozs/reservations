import React  from 'react'

function HoursOption(props) {
    let renderedOptions = [];   
    let disableIdx = null;
    

     if(typeof props.options[0] === 'object' && props.target === "to"){
      
            const startTime = parseInt(props.fromTime);
            const filteredArray = props.options.slice( startTime  + 1)
            const minDisabledTime  = filteredArray.findIndex(t => t.reserveStart);


            if(minDisabledTime > -1 ){
              const foundIdx =  minDisabledTime + startTime +  1;
              renderedOptions =  props.options.map((t,i) =>  i <= foundIdx && i > startTime  ?<option key={i}  value={i}>{t.time}</option> : null) 
            }else{
              renderedOptions =  props.options.map((t,i) =>  i > startTime ?<option key={i}  value={i}>{t.time}</option> : null) 
            }
     }else{


     
     
      renderedOptions =  props.options.map( (t,i) =>{
        
            if(typeof t === 'object'){
                      // && props.target === "from"
                 if(t.reserveStart  ){
                          disableIdx = t.reserveEnd;
                          return <option key={i} disabled={true} value={i}>{t.time}</option>
                  }

                  if( disableIdx && i < disableIdx   ){
                        return <option value={i} key={i} disabled={true}>{t.time}</option>
                  }

                  if(disableIdx && disableIdx === i ){
                    disableIdx = null;
                  }

                  return <option  value={i} key={i}>{t.time}</option>

            }else{
              if(props.target === "to"){
                  return   i > props.fromTime ? <option value={i} key={i} >{t}</option> : null;
              }else{
                return <option value={i} key={i}>{t}</option>
              }
              
            }

      })


    }
    if(!renderedOptions.length || renderedOptions.every(t => t === null)){
                     return <option selected="selected" disabled={true} value="0">Device full</option>
    }

    return renderedOptions;
 
  
}

export default HoursOption;