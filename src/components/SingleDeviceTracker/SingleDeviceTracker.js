import React from 'react'
import './SingleDeviceTracker.scss';
import { HoursFullTimeline  , getClassOfPoint } from '../../utils';
import { observer } from "mobx-react-lite";


function SingleDeviceTracker({reservations , date , currentUserID}) {
  return (<div className="SingleDeviceTracker">
              <div className="SingleDeviceTracker-inner-tracker">
                      {HoursFullTimeline.map( (r,i) => <div className="track-line" key={i}></div>)}
              </div>
        
        {
          reservations && reservations[date] ?  
          reservations[date].map( (r,i) => (<div key={i} className={`point ${getClassOfPoint(r,currentUserID, i)}`} style={r.reserveStart === true ?  { "--startPoint" : i + 2 , "--endPoint" : r.reserveEnd + 2} : null}></div>) ) : HoursFullTimeline.map( (r,i) => <div key={i}></div>) 
        }
        
  </div>)
}

export default observer(SingleDeviceTracker);