import React from 'react'
import './BtnAddReservation.scss';

export default function BtnAddReservation({text , onClick  , target}) {
  return (
        <button className="BtnAddReservation" onClick={onClick} data-target={target}>{text}</button>
  )
}
