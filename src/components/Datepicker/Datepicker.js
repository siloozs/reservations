import React from "react";
import "./Datepicker.scss";
import observableDeviceStore from '../../Store';
import { observer } from "mobx-react-lite";



const Datepicker = props => {
  
  return (
    <div className="datepicker">
      <button className="btn" data-direction="l" onClick={evt => observableDeviceStore.setDate(evt)}>
        <span className="arrow arrow--left"></span>
      </button>
      <div className="current-date"> {observableDeviceStore.currentDate}</div>
      <button className="btn" data-direction="r" onClick={evt => observableDeviceStore.setDate(evt)}>
        <span className="arrow arrow--right"></span>
      </button>
    </div>
  );
};

export default observer(Datepicker);


