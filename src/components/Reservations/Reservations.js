import React , { useState , useCallback } from "react";
import "./Reservations.scss";
import Datepicker from "../Datepicker/Datepicker";
import Timeline from "../Timeline/Timeline";
import BtnAddReservation from '../BtnAddReservation/BtnAddReservation';
import Popup from '../Popup/Popup';


const Reservations = () => {
  const [showpopup , setShowPopup ] = useState(false);
  const changePopupState = useCallback( evt => {
          if(evt instanceof Object){
            evt.preventDefault();
            setShowPopup( evt.currentTarget.dataset.target === 'true' );
          }else{
            setShowPopup(false);
          }
           
  }, [])


  return (
    <div className="reservations">
      {showpopup && <Popup onClick={changePopupState} target={false} />}
      <div className="reservations-header">
          <Datepicker />
          <BtnAddReservation text="+ Add Reservation" onClick={changePopupState} target={true} />
      </div>
      <Timeline />
    </div>
  );
};

export default Reservations;
