import { useState }from 'react';

export const devices = [
  {
    name: "HTC One M8",
    serial: "HT519WM00429",
    reservations:null
  },
  {
    name: "Motorola Moto X 2014",
    serial: "TA9890AQPQ",
    reservations:null
  }
];

export const DATE_FORMAT = 'MMM DD ,YYYY';

export const useInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(""),
    bind: {
      value,
      onChange: event => {
        setValue(event.target.value);
      }
    }
  };
};



export const HoursTimeline = Array.from(
  Array(24),
  (x, i) => ("0" + (23 - i)).slice(-2) + ":00"
).reverse();

export const HoursFullTimeline = [];

HoursTimeline.forEach((h) => {
  HoursFullTimeline.push(h, h.substring(0, 2) + ":30");
});




export const getClassOfPoint = (reservation , currentUserId , idx) =>{
  if(reservation.reserveStart ){
        return currentUserId  === reservation.userId ? 'currentUserColor' : 'others'
  }
   return '';
}
