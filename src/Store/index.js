import { makeAutoObservable , observable  , action  , toJS  , computed} from "mobx"
import { HoursFullTimeline , devices ,  DATE_FORMAT } from '../utils/';
import moment from 'moment';


class DeviceStore{
  devices = []
  currentDate =  moment().format(DATE_FORMAT);
  loggedUserId = 1
  fullyOccupied = {}


    constructor(){
          makeAutoObservable(this, {
            devices: observable,
            currentDate: observable,
            loggedUserId: observable,
            fullyOccupied:observable,
            setDate:action,
            setDevices:action,
            setReservation: action,
            currentDateObj:computed
        });
         
    }

    get currentDateObj(){
          return new Date(this.currentDate);
    }

    setDevices(devices){
      this.devices.push(...devices);
    }

    setReservation(idx , data){
      if(!this.devices[idx].reservations ){
        
        this.devices[idx].reservations = {};
        this.devices[idx].reservations[this.currentDate] =  HoursFullTimeline.map((t,i) => {
                    return {
                      time:t,
                      reserveStart:i === data.i,
                      reserveEnd: data.idxEnd ? data.idxEnd : null,
                      userId: this.loggedUserId
                    }
        })
        }else{
              if( ! Array.isArray(this.devices[idx].reservations[this.currentDate])  ){
                     this.devices[idx].reservations[this.currentDate] = HoursFullTimeline.map((t,i) => {
                            return {
                              time:t,
                              reserveStart:i === data.i,
                              reserveEnd: data.idxEnd ? data.idxEnd : null,
                              userId: this.loggedUserId
                            }
                       })
              }else{

                this.devices[idx].reservations[this.currentDate][data.i] = {
                  time: HoursFullTimeline[data.i] ,
                  reserveStart:true,
                  reserveEnd:  data.idxEnd,
                  userId: this.loggedUserId
                }

              }

             

              
        }

    }

 
    fulltimelineDevice(deviceID){
      if(this.devices[deviceID].reservations  &&  this.devices[deviceID].reservations[this.currentDate]){
              return toJS(this.devices[deviceID].reservations[this.currentDate]);
        }else{
          return HoursFullTimeline
        }
    }

    setDate(data){
          if(data instanceof Date){
            this.currentDate =  moment(data).format(DATE_FORMAT);
          } else if(data.currentTarget.dataset.direction === "l"){
            this.currentDate =  moment(new Date(this.currentDate)).subtract(1, 'day').format(DATE_FORMAT);
          }else{
            this.currentDate = moment(new Date(this.currentDate)).add(1, 'day').format(DATE_FORMAT);
          }
       
    }

}

const observableDeviceStore = new DeviceStore();
observableDeviceStore.setDevices(devices);



export default observableDeviceStore;